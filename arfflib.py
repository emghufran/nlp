'''
This file defines functions for reading and writing to arff files. It uses the package liac-arff-1.0
http://pypi.python.org/pypi/liac-arff

'''
import arff

class ArffFile:
    filename = ""
    attributes = []
    data = {}
    description = ""
    relation = ""

    def __init__(self, filename = "", attributes = [], data = {}, description = "", relation = ""):
        self.filename = filename
        self.attributes = attributes
        self.data = data

    def read_file(self, filename):
        ''' Reads a file if filename is provided as argument. updates the filename and calls the generic function'''
        self.filename = filename
        read_file(self)

    def read_file(self):
        '''uses the liac-arff package to read the file and store them here'''
        read_data = arff.load(open(self.filename, 'rb'))
        self.attributes = read_data['attributes']
        self.data = read_data['data']
        if 'description' in read_data:
            self.description = read_data['description']
        
        if 'relation' in read_data:
            self.relation = read_data['relation']

    def write_file(self, filename, replace_name = False):
        if replace_name == True:
            self.filename = filename

        #self.description = "this is description"
        print(self.attributes)
        data_to_write = {'attributes': self.attributes, 'data': self.data, 'description' : self.description, 'relation': self.relation}
        #print(arff.dumps(data_to_write))
        outfile = open(filename, 'w')
        outfile.write(arff.dumps(data_to_write))

#
#f = ArffFile()
#f.filename = "example.arff"
#f.read_file()
#print f.attributes
#print f.description
#if.write_file("out_example.arff")
