#This will contain the feature extractor
#Required package: nltk

import nltk
import os
import arfflib
import pickle
import string
from nltk.stem.lancaster import LancasterStemmer
from nltk.tokenize import word_tokenize, wordpunct_tokenize, sent_tokenize
import math
import codecs


feature_names = ['limited', 'national', 'high', 'college', 'campus', 'industries', 'united', 'group', 'service', 'corporation', 'research', 'program', 'stadium', 'international', 'business', 'club', 'football', 'baseball', 'student', 'league', 'school', 'rugby', 'institute', 'university', 'technolog', 'ltd'] 
#PICKLE STUFF#
class REDUCEDPAGE():
  def __init__(self,name):
    self.name=name
    self.alternateNames=[]
    self.infoboxes=[]
    self.sentences=[]
    self.cats=[]
  def describe(self):
    print('name '+self.name)
    print('alternateNames '+str(self.alternateNames))
    print('infoboxes '+str(self.infoboxes))
    print('sentences '+ str(self.sentences))
    print('categories '+ str(self.cats))

def loadPickleReducedPages(ent):
    global reducedPages
    ent=open(ent,'rb')
    reducedPages=pickle.load(ent)
    ent.close()

#loadPickleReducedPages("/Users/brindusasmaranda/Desktop/master courses 2011-2013 /second year/case study/case study NLP/reducedPages.pic")
#loadPickleReducedPages("data/reducedPages.pic")

#PICKLE STUFF ENDS#

def contain_word(text, word):
  ''' returns true if word is found in the text (if string) or all the lists '''
  if isinstance(text, basestring):
    return word in text
  else:
    for line in text:
      if word in line.lower():
        return True
  return False

def count_word_freq(text, word):
  ''' (file open for reading, str) -> boolean
  Return count of word in the open file
  Precondition: -
  '''
  count = 0
  if isinstance(text, basestring):
    count = text.count(word)
  else:
    for line in text:
      count += line.count(word)
  return count

def contain_numeric(text):
  ''' (file open for reading) -> boolean
  Return TRUE if there is number contained in the open file
  Precondition: -
  '''
  import re
  match_obj = None
  if isinstance(text, basestring):
    match_obj = re.search(r'[0-9]', text)
  else:
    for line in text:
      match_obj = re.search(r'[0-9]', line)
      if match_obj:
        return True
  if match_obj == None:
    return False

def count_word(text):
  ''' (file open for reading) -> int
  Return the number of word in the file
  Precondition: -
  '''
  
  if isinstance(text, basestring):
    tempwords = nltk.word_tokenize(text)
    return len(tempwords)
  else:
    count = 0
    for line in text:
        tempwords = nltk.word_tokenize(line)
        count = count + len(tempwords)
    return count

def count_cha(text):
  ''' (file open for reading) -> int
  Return the number of characters in the file
  Precondition: -
  '''

  if isinstance(text, basestring):
    return len(text)
  else:
    count = 0
    for line in text:
        for s in line:        
            count += 1
    return count

def tag_POS(textfile):
    ''' (file open for reading) -> list of (word, POS)

    Return the list of (word, POS) from the file

    Precondition: -
    '''
    res = []
    for line in text:
        words = nltk.word_tokenize(line)
        res = res + nltk.pos_tag(words)
    return res

def get_feature_list(title, text):
  ''' (file open for reading) -> list of features
    Return the list of features from the file
    Precondition: -
  '''
  '''
  add name, type and possible values (where applicable) here so its easy to quickly check

  [("wc_text", "REAL"), ("cc_text", "REAL"), ("contains_numeric", "NOMINAL", ['TRUE', 'FALSE']),("wc_title", "REAL"), ("cc_title", "REAL"), 
  ("contains_numeric_title", "NOMINAL", ['TRUE', 'FALSE']), ("cont_club_title", "NOMINAL", ['TRUE', 'FALSE']), 
  ("cont_company_title", "NOMINAL", ['TRUE', 'FALSE']), ("cont_group_title", "NOMINAL", ['TRUE', 'FALSE']), ("cont_inc_title", "NOMINAL", ['TRUE', 'FALSE']), 
  ("cont_team_title", "NOMINAL", ['TRUE', 'FALSE']), ("cont_university_title", "NOMINAL", ['TRUE', 'FALSE']),
  
  ("freq_club", "REAL"), ("freq_company", "REAL"), ("freq_group", "REAL"), ("freq_inc", "REAL"), 
  ("freq_team", "REAL"), ("freq_university", "REAL")]



  '''
  feature_list = []
  #add features for title here

  #add features for text part here. DONT MODIFY ORDER WITHOUT MODIFYING ORDER IN FILE WRITING CODE
  
  global feature_names  
  
  feature_list.append(count_word(text))
  feature_list.append(count_cha(text))
  feature_list.append(contain_numeric(text))
  feature_list.append(count_word(title))
  feature_list.append(count_cha(title))
  feature_list.append(contain_numeric(title))
  
  for feat in feature_names:
    feature_list.append(contain_word(title, feat))

  for feat in feature_names:
    feature_list.append(count_word_freq(text, feat))
  
  #feature_list.append(contain_word(title, "club"))
  #feature_list.append(contain_word(title, "company"))
  #feature_list.append(contain_word(title, "group"))
  #feature_list.append(contain_word(title, "inc"))
  #feature_list.append(contain_word(title, "team"))
  #feature_list.append(contain_word(title, "university"))
  
  #feature_list.append(count_word_freq(text, "club"))
  #feature_list.append(count_word_freq(text, "company"))
  #feature_list.append(count_word_freq(text, "group"))
  #feature_list.append(count_word_freq(text, "inc"))
  #feature_list.append(count_word_freq(text, "team"))
  #feature_list.append(count_word_freq(text, "university"))

  return feature_list

#Deprecated
def combine(directory):
    ''' list -> list of lists

    Return the list of feature lists for each document

    Precondition: -
    '''
    all_list = []
    d=os.listdir(directory)
    for f in d:
        filename = f
        if(os.path.isfile(f)):
            textfile = open(directory + '/' + filename,'r')
            l = get_feature_list(textfile)
            all_list.append(l)
    return all_list

def get_attributes_header():

  global feature_names
  feature_header = [("wc_text", "REAL"), ("cc_text", "REAL"), ("contains_numeric", "{True, False}"),("wc_title", "REAL"), ("cc_title", "REAL"), 
      ("contains_numeric_title", "{True, False}")]
  for feat in feature_names:
    #feat_tuple = "cont_" + feat + "_title", "NOMINAL, ['TRUE', 'FALSE']"
    feat_tuple = "cont_" + feat + "_title", "{True, False}"
    feature_header.append(feat_tuple)

  for feat in feature_names:
    feat_tuple = "freq_" + feat, "REAL"
    feature_header.append(feat_tuple)
  return feature_header
  
def write_to_file(list_of_features, class_names, description = "", output_file = "arfffile.arff", relation = ""):
    #list_of_features = combine(directory)
    f = arfflib.ArffFile()
    #f.attributes = [("wc_text", "REAL"), ("cc_text", "REAL"), ("contains_numeric", "NOMINAL", ['TRUE', 'FALSE']),("wc_title", "REAL"), ("cc_title", "REAL"), 
    #  ("contains_numeric_title", "NOMINAL", ['TRUE', 'FALSE']), ("cont_club_title", "NOMINAL", ['TRUE', 'FALSE']), 
    #  ("cont_company_title", "NOMINAL", ['TRUE', 'FALSE']), ("cont_group_title", "NOMINAL", ['TRUE', 'FALSE']), ("cont_inc_title", "NOMINAL", ['TRUE', 'FALSE']), 
    #  ("cont_team_title", "NOMINAL", ['TRUE', 'FALSE']), ("cont_university_title", "NOMINAL", ['TRUE', 'FALSE']),
    #  ("freq_club", "REAL"), ("freq_company", "REAL"), ("freq_group", "REAL"), ("freq_inc", "REAL"), 
    #  ("freq_team", "REAL"), ("freq_university", "REAL")]

    f.attributes = get_attributes_header()

    f.attributes.append(("class", "{" + ", ".join(class_names) + "}" )) #dynamically adding class names 

    f.data = list_of_features
    f.description = "test_description" if description == "" else description
    f.relation = "WikipediaFeatures" if relation == "" else relation
    f.write_file(output_file)

#Deprecated
def read_features(sample_categories, raw_data):
    f = open(sample_categories,"r")
    for line in f:
        file_text = raw_data[line]
        features = get_feature_list(line, file_text)

def process_filename(filename):
  return string.replace(filename.split('/')[-1], ".txt", "")

def import_from_pickle(pickle_path, index_files):
  class_names = [process_filename(i) for i in index_files]

  loadPickleReducedPages(pickle_path)
  features_list = []
  count = 0
  for infile in index_files:
    f = open(infile, "r")
    this_file_class = process_filename(infile)
    for page_name in f:
      title = page_name.strip()
      try:
        reduced_page = reducedPages[title]
        one_file_features = get_feature_list(title, reducedPages[title].sentences)
        one_file_features.append(this_file_class)
        features_list.append(one_file_features)
      except KeyError:
        print("KeyError: ", title)

  write_to_file(features_list, class_names, "Features for Classifying Wikipedia Articles")

class FeatureExtractor:
    features = [("word_count", "REAL"), ("character_count", "REAL"), ("club", "REAL"), ("company", "REAL"), 
            ("group", "REAL"), ("inc", "REAL"), ("team", "REAL"), ("university", "REAL")]




#########################################################
#filename = 'test.txt'
#textfile = open(filename, 'r')
#directory = '/users/punyaveenapalai/code/nlp/input'

index_files = ["data/COMPANY.txt", "data/TEAM.txt", "data/PARTY.txt", "data/UNIVERSITY.txt"]
import_from_pickle("data/reducedPages.pic", index_files)

#print process_filename("data/COMPANY.txt")

################ Run time is very long ###################

def get_titles_from_pickle(pickle_path, index_files):
  loadPickleReducedPages(pickle_path)
  bag = []
  list_titles = []
  for infile in index_files:
    f = open(infile, "r")
    this_file_class = process_filename(infile)
    for page_name in f:
      title = page_name.strip()

      try:
        chars = ['_',',','(',')','\\','"',"'","+","-",".","/","@","&","$","#","!","%","*"]
        reduced_page = reducedPages[title]
        for c in chars:
          title = title.replace(c, u' ')
        title = title.upper()
        title = title.split()
        title = [x for x in title if len(x)>2]
        list_titles.append(title)
        for t in title:
          if t not in bag:
            bag.append(t)
        #bag = bag + title
        
      except KeyError:
        print("KeyError: ", title)

  chars = ['_',',','(',')','\\','"',"'","+","-",".","/","@","&","$","#","!","%","*"]
  term=sorted(list(set(bag)))
  tf=[float(bag.count(t))/len(bag) for t in term]
  idf=[]
  for t in term:
    count = 0
    for i in range(len(list_titles)):
      if t in list_titles[i]: count += 1
    #idf.append(math.log(float(len(reducedPages.keys()))/count))
    idf.append(math.log(float(len(list_titles))/count))

  tfidf = [float(tf[i])*idf[i] for i in range(len(tf))]
  #rank=sorted(tfidf,reverse=True)
  #print rank[0:100]
  #position=[]
  #for r in rank[0:200]:
  #  m = r
  #  position.extend([i for i, j in enumerate(tfidf) if j == m])

  combined = zip(tfidf, term)
  combined.sort(reverse=True)
  print combined
  #interest_title_terms = [term[p] for p in position]

  #print interest_title_terms[0:100]
  #return interest_title_terms
  return combined

def update_tf_idf_for_document(sentences, tf, idf, is_text = False):
  chars = ['_',',','(',')','\\','"',"'","+","-",".","/","@","&","$","#","!","%","*"]
  this_doc_terms = set()
  for sent in sentences:
    s = sent
    for c in chars:
      s = s.replace(c, u' ')
    s = s.upper().split()
    for t in s:
      this_doc_terms.add(t)
      if t in tf:
        tf[t] += 1
      else:
        tf[t] = 1

  for t in this_doc_terms:
    if t in idf:
      idf[t] +=1
    else:
      idf[t] = 1
  return tf, idf
  
def get_top_words_from_title(pickle_path, index_files, output_file):
  loadPickleReducedPages(pickle_path)
  tf = {}
  idf = {}
  doc_count = 0
  for infile in index_files:
    f = codecs.open(infile, "r", "utf8")
    this_file_class = process_filename(infile)
    for page_name in f:
      doc_count += 1
      title = page_name.strip()
      try:
        chars = ['_',',','(',')','\\','"',"'","+","-",".","/","@","&","$","#","!","%","*"]
        title = title.upper()
        sentences = [title]
        update_tf_idf_for_document(sentences, tf, idf)
      except KeyError:
        print("KeyError: ", title)
  
  #print(len(idf))
  tfidf = {}
  for key, value in tf.iteritems():
    tfidf[key] = tf[key] * math.log(doc_count/idf[key])
  import operator 
  sorted_tfidf = sorted(tfidf.iteritems(), key=operator.itemgetter(1), reverse=True)
  #outfile = codecs.open(output_file, "w+", "utf-8")
  printed_count = 0
  for itm in sorted_tfidf:
    printed_count += 1
    if printed_count > 100:
      break
    to_write = str(itm[0].encode('utf8')) + ", " + str(itm[1])
    print(to_write)
    #outfile.write(to_write)
  #outfile.close()

def get_top_words_from_text(pickle_path, index_files, output_file):
  loadPickleReducedPages(pickle_path)
  tf = {}
  idf = {}
  doc_count = 0
  for infile in index_files:
    f = open(infile, "r")
    this_file_class = process_filename(infile)
    for page_name in f:
      doc_count += 1
      title = page_name.strip()
      try:
        sentences = reducedPages[title].sentences
        update_tf_idf_for_document(sentences, tf, idf, True)
      except KeyError:
        print("KeyError: ", title)
  
  tfidf = {}
  for key, value in tf.iteritems():
    tfidf[key] = tf[key] * math.log(doc_count/idf[key])
  import operator 
  sorted_tfidf = sorted(tfidf.iteritems(), key=operator.itemgetter(1), reverse=True)
  #outfile = codecs.open(output_file, "w+")
  printed_count = 0
  for itm in sorted_tfidf:
    printed_count += 1
    if printed_count > 100:
      break
    to_write = str(itm[0].encode('utf8')) + ", " + str(itm[1])
    print(to_write)
  #outfile.close()

#uncomment to run tfidf. each file needs to be run independently. 
#this is dirty code, but unicode is causing a lot of problems.. 
'''

print("starting tfidf")
company_files = ["data/COMPANY.txt"] #, "data/TEAM.txt", "data/PARTY.txt", "data/UNIVERSITY.txt"]
all_files = ["data/COMPANY.txt", "data/TEAM.txt", "data/PARTY.txt", "data/UNIVERSITY.txt"]
team_files = ["data/TEAM.txt"]
party_files = ["data/PARTY.txt"]
university_files = ["data/UNIVERSITY.txt"]

#company_interest_terms = get_titles_from_pickle("data/reducedPages.pic", company_files)
#team_interest_terms = get_titles_from_pickle("data/reducedPages.pic", team_files)
#party_interest_terms = get_titles_from_pickle("data/reducedPages.pic", party_files)
#university_interest_terms = get_titles_from_pickle("data/reducedPages.pic", university_files)

#get_top_words_from_title("data/reducedPages.pic", company_files, "company_tfidf.txt")
#get_top_words_from_title("data/reducedPages.pic", team_files, "team_tfidf.txt")
#get_top_words_from_title("data/reducedPages.pic", party_files, "party_tfidf.txt")
#get_top_words_from_title("data/reducedPages.pic", university_files, "university_tfidf.txt")

#get_top_words_from_text("data/reducedPages.pic", company_files, "company_tfidf_text.txt")
#get_top_words_from_title("data/reducedPages.pic", all_files, "all_tfidf_title.txt")
'''
def write_interesting_to_file(obj, filename):
  f = open(filename, "w+")
  for o in obj:
    to_write = str(o[1]) +", " + str(o[0]) + "\n"
    f.write(to_write)
  f.close()

#write_interesting_to_file(company_interest_terms, "company_interesting.txt")
#write_interesting_to_file(team_interest_terms, "team_interesting.txt")
#write_interesting_to_file(party_interest_terms, "party_interesting.txt")
#write_interesting_to_file(university_interest_terms, "university_interesting.txt")
#print("Writing interesting terms done")
#print company_interest_terms

# TF-IDF
# title
'''
bag = []
list_titles = []
for i in range(len(reducedPages.keys())):
  title = reducedPages.keys()[i]
  title = title.translate(dict((ord(c), u' ') for c in chars))
  title = title.upper()
  title = title.split()
  title = [x for x in title if len(x)>2]
  list_titles.append(title)
  bag = bag + title

chars = ['_',',','(',')','\\','"',"'","+","-",".","/","@","&","$","#","!","%","*"]
term=sorted(list(set(bag)))
tf=[float(bag.count(t))/len(bag) for t in term]
idf=[]
for t in term:
  count = 0
  for i in range(len(list_titles)):
    if t in list_titles[i]: count += 1
  #idf.append(math.log(float(len(reducedPages.keys()))/count))
  idf.append(math.log(float(len(list_titles))/count))

tfidf = [float(tf[i])*idf[i] for i in range(len(tf))]
rank=sorted(tfidf,reverse=True)

position=[]
for r in rank[0:200]:
  m = r
  position.extend([i for i, j in enumerate(tfidf) if j == m])

interest_title_terms = [term[p] for p in position]

print interest_title_terms
'''

'''
# content
sentences=[]
st = LancasterStemmer()
titles = sorted(reducedPages.keys())

for title in titles:
  sentences.append(reducedPages[title].sentences)

doc_dict = {title:[] for title in titles}
for doc in range(len(reducedPages.keys())):
  for s in sentences[doc]:
    s = s.translate(dict((ord(c), u' ') for c in chars))
    s = s.upper()
    s = wordpunct_tokenize(s)
    s = [x for x in s if len(x)>2]
    s = [st.stem(x) for x in s]
    doc_dict[titles[doc]] = doc_dict[titles[doc]] + s


bag = []
for k in doc_dict:
  bag = bag + doc_dict[k]

term=sorted(list(set(bag)))
tf=[float(bag.count(t))/len(bag) for t in term]
idf=[]
for t in term:
  count = 0
  for k in doc_dict:
    if t in doc_dict[k]: count += 1
  idf.append(math.log(float(len(reducedPages.keys()))/count))

tfidf = [float(tf[i])*idf[i] for i in range(len(tf))]
rank=sorted(tfidf,reverse=True)

position=[]
for r in rank[0:200]:
  m = r
  position.extend([i for i, j in enumerate(tfidf) if j == m])

interest_title_terms = [term[p] for p in position]
'''
