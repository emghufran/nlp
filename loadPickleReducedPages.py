# -*- coding: utf8 -*-
#!/pkg/ldc/bin/python2.5
#-----------------------------------------------------------------------------
# Name:        pln20122013.py
# Purpose:
#
# Author:      Horacio
#
# Created:     2012/09/06
# auxiliar 
#-----------------------------------------------------------------------------

import pickle

class REDUCEDPAGE():

    def __init__(self,name):
        self.name=name
        self.alternateNames=[]
        self.infoboxes=[]
        self.sentences=[]
        self.cats=[]
    def describe(self):
        print('name '+self.name)
        print('alternateNames '+str(self.alternateNames))
        print('infoboxes '+str(self.infoboxes))
        print('sentences '+ str(self.sentences))
        print('categories '+ str(self.cats))

def loadPickleReducedPages(ent):
    global reducedPages
    ent=open(ent,'rb')
    reducedPages=pickle.load(ent)
    ent.close()

#loadPickleReducedPages("/Users/brindusasmaranda/Desktop/master courses 2011-2013 /second year/case study/case study NLP/reducedPages.pic")
loadPickleReducedPages("data/reducedPages.pic")

#a=list(reducedPages)
#print len(a)
#print(a[0])

p=reducedPages["Economy_of_Namibia"]
p=reducedPages["Incepta_Pharmaceuticals_Limited"]
print(len(p.sentences))
#print(reducedPages.keys()[8000:11000])
[i for i in reducedPages.keys() if i in ['Corporaci']]
#p.describe()
